package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();

		/*
		 * Falla el bucle for al recorrer la cadena
		 * se indica <= en la segunda componente del bucle por lo que a una
		 * cadena de cuatro caracteres (0,1,2,3), le estamos diciento que 
		 * busque hasta la longitud total de caracteres que lee el metodo .lenght,
		 * o sea 4 (0,1,2,3,4). Al buscar el bucle for un caracter que no existe
		 * el bucle devuelve una excepcio
		 * 
		 * se solucionaria eliminando el = del recorrido de candena.lenght
		 * for (int i = 0; i <= cadenaLeida.length(); i++) {
		 */
		
		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
